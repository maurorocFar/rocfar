import { BrowserRouter, Route, Routes } from "react-router-dom";

import Home from "./pages/Home";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import { LanguageProvider } from "./Providers/LanguajeProvider";
import { ContextProvider } from "./Providers/global.context";



export default function App() {
  return (
    <>
      <ContextProvider>
      <LanguageProvider>
      <BrowserRouter>
        <Navbar/>
        <Routes>
          <Route exact path="/" element={<Home />} />
          {/* Agrego algo com */}

        </Routes>
        <Footer/>
      </BrowserRouter>
      </LanguageProvider>
      </ContextProvider>
  
    </>
  );
}
