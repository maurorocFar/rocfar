import { useState, useContext } from "react";
import { LanguageContext } from "../Providers/LanguajeProvider";
import axios from 'axios';
import messagesES from "../js/es";
import messagesEN from "../js/en";
import messagesFR from "../js/fr";


const Form = () => {

  const { language } = useContext(LanguageContext);
  let messages = {};

  if (language === "es") {
    messages = messagesES;
  } else if (language === "en") {
    messages = messagesEN;
  } else if (language === "fr") {
    messages = messagesFR;
  }

  //Aqui deberan implementar el form completo con sus validaciones
  const [nombreCompleto, setNombreCompleto] = useState("");
  const [email, setEmail] = useState("");
  const [consulta, setConsulta] = useState("");

  // este useState es para manejar el div de formulario enviado con exito
  const [isSubmitted, setIsSubmitted] = useState(false);

  //campos vacios

  const [camposVacios, setCamposVacios] = useState(false);

  //email formato incorrecto

  const [validEmail, setValidEmail] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
  
    //validacion formulario
    if (
      nombreCompleto.trim() === "" ||
      email.trim() === "" ||
      consulta.trim() === ""
    ) {
      setCamposVacios(true);
      setTimeout(() => {
        setCamposVacios(false); // Cambia el estado a false después de 2 segundos
      }, 2000);
      return;
    }
  
    // Validación del formato de email
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setValidEmail(true);
      setTimeout(() => {
        setValidEmail(false); // Cambia el estado a false después de 2 segundos
      }, 2000);
  
      return;
    }
  
    //---------------------------------------------------------------------------------------------
    //conexion base de datos
  
    axios.post('http://191.101.234.226/rocfar', {
      nombreCompleto: nombreCompleto,
      email: email,
      consulta: consulta
    })
      .then(response => {
        console.log(response.data); // Muestra la respuesta del servidor en la consola
        setIsSubmitted(true); // Establece la variable de estado a true cuando se envía el formulario
        setTimeout(() => {
          setIsSubmitted(false); // Cambia el estado a false después de 4.5 segundos
        }, 4500);
      })
      .catch(error => {
        console.error('Error:', error);
      });
  
    console.log("enviando");
    console.log("Su nombre es", nombreCompleto);
    console.log("Su email es", email);
    console.log("Su consulta es", consulta);
  };
  
  return (
    <>
      <div className="div">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 ">
              <form onSubmit={handleSubmit} className="text-center">
                {/* nombre completo */}
                <label>{messages.ingresName}</label>

                <div>
                  <input
                    id="nombreCompleto"
                    type="string"
                    placeholder={messages.ingresName}
                    value={nombreCompleto}
                    onChange={(e) => setNombreCompleto(e.target.value)}
                    className="inputForm"
                  />
                </div>
                {/* email */}
                <label>{messages.ingresEmail}</label>
                <div>
                  <input
                    id="email"
                    type="string"
                    placeholder={messages.ingresEmail}
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    className="inputForm"
                  />
                </div>
                {/* consulta */}

                <label>{messages.ingresConsul}</label>

                <div>
                  <textarea
                    id="consulta"
                    type="text"
                    placeholder={messages.ingresConsul}
                    value={consulta}
                    onChange={(e) => setConsulta(e.target.value)}
                    className="consulForm"
                  />
                </div>
                <div>
                  <button onSubmit={handleSubmit}>{messages.enviar}</button>
                </div>
              </form>
            </div>
            <div className="col-12">
              {/* si se envia con exito */}
              {isSubmitted && (
                <div className="bg-exito text-white text-center pb-3 mt-2">
                {messages.sendExit}
                </div>
              )}

              {/* si los campos estan vacios */}

              {camposVacios && (
                <div className="bg-fail text-white text-center pb-3 mt-2">
                  {messages.camposVacios}
                </div>
              )}

              {validEmail && (
                <div className="bg-fail text-white text-center pb-3 mt-2">
                 {messages.validEmail}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Form;
