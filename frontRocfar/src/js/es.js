
const messages = {
    title: "Bienvenidos",
    //Aqui comienzan las variables Sobre Nosotros
    aboutme: "Sobre Nosotros",
    p1: "Nacidos en el año 2022, nuestras bases se fundamentan en dar soluciones digitales a problematicas cotidianas que se presentan en la sociedad.",
    p2: "Con el uso de tecnologias, se pretende potenciar al individuo, ya sea ampliando sus clientes o mejorando sus servicios si es que posee un negocio, como asi tambien, dando inicio a nuevos proyectos con un solido marco digital.",
    p3: "Sin duda alguna, la tecnología ha avanzado de manera vertiginosa en los últimos tiempos y ha llegado para quedarse. En este contexto, resulta evidente que aquellos negocios que deciden implementar tecnologías digitales tienen mayores probabilidades de experimentar un crecimiento significativo. La adopción de soluciones tecnológicas se ha convertido en una ventaja competitiva clave en el mercado actual, permitiendo optimizar procesos, mejorando la eficiencia y ofrececiendo experiencias más personalizadas a los clientes.",
    p4a:"En ultima instancia desde",
    p4b: "se trabaja en proyectos propios, los cuales nacen a partir de la vision de impulsar la vida de las personas, al logro de objetivos, establecimiento de metas y cumplimiento de propositos.",
    //-----------------------------------------------

    //Comienzo Variables Footer

    tecnologies: "Tecnologias",
    redes: "Redes",
    derechos: "Derechos de Autor ©",
    tel: "tel", 

    //---------------------------
    //Comienzo variables NavBAr

    inicio: "Inicio",
    about: "Sobre Nosotros",
    contact: "Contacto",

    //---------------------------
    //Carrusel textos

    img1a:"A nosotros tambien nos interesa",
    img1b:"que mas personas conozcan lo que haces",
    img2a:"Los servicios Necesarios para que tengas tu mejor sitio Web",
    img3a:"Soluciones integrales y adaptables a tu necesidad",
    img3b:"Dejanos sumarnos a tu crecimiento",


    //--------------------------
    //contacto and text

    texto1andwsap:"Si consideras que podemos ayudarte, no dudes en contactarnos sin compromiso ",

    //------------------------------------------------------------------------


    //text Home de Home

    servicios:"Servicios",
    textLi1:"Web adaptable a todos los dispositivos",
    textLi2:"Actualizacion de contenidos<",
    textLi3:"Gestion Integral de dominios",
    textLi4:"Email Profesional (@tuDominio)",
    textLi5:"Aplicaciones Web",
    prestador:"Como prestador de servicios digitales, nuestro objetivo es ayudar al emprendedor a mejorar su presencia digital y alcanzar sus objetivos en línea. Esto puede incluir diseños web, marketing digital, publicidad en redes sociales, optimización de motores de búsqueda (SEO), gestión de reputación en línea y mucho más.",

    //---------------------------


    ingresName:"Ingrese su nombre",
    ingresEmail:"Ingrese su email",
    ingresConsul:"Ingrese su consulta",

    enviar:"enviar",
    sendExit: "El formulario se ha enviado con éxito , a la brevedad responderemos su inquietud.",
    camposVacios: "Ningun Campo puede permanecer vacio",
    validEmail:"El formato del email es invalido, verifique y vuelva a intentarlo",

    // otras cadenas de texto en español
  };
  
  export default messages;
  