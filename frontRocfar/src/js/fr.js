// fr.js
const messages = {
    title: "Accueillir",
    aboutme: "À propos de nous",
    p1: "Nées en 2022, nos bases sont basées sur l'apport de solutions numériques aux problèmes quotidiens qui se posent dans la société.",
    p2:"Avec l'utilisation des technologies, il est destiné à responsabiliser l'individu, soit en élargissant sa clientèle, soit en améliorant ses services s'il a une entreprise, ainsi qu'en démarrant de nouveaux projets avec un cadre numérique solide.",
    p3: "Sans aucun doute, la technologie a progressé rapidement ces derniers temps et est là pour rester. Dans ce contexte, il est évident que les entreprises qui décident de mettre en œuvre les technologies numériques sont plus susceptibles de connaître une croissance importante. L'adoption de solutions technologiques est devenue un avantage concurrentiel clé sur le marché actuel, permettant d'optimiser les processus, d'améliorer l'efficacité et d'offrir des expériences plus personnalisées aux clients.",
    p4a: "Enfin depuis",
    p4b: "Nous travaillons sur nos propres projets, qui naissent de la vision de promouvoir la vie des gens, d'atteindre des objectifs, de fixer des objectifs et d'accomplir des objectifs.",
        //-----------------------------------------------

    //Comienzo Variables Footer

    tecnologies: "Les technologies",
    redes: "réseaux sociaux",
    derechos: "Droit d'auteur ©",
    tel: "téléphone", 


        //---------------------------
    //Comienzo variables NavBAr

    inicio: "Accueil",
    about: "À propos de nous",
    contact: "Contact",

       //---------------------------
    //Carrusel textos

    img1a:"Nous sommes également intéressés",
    img1b:"que plus de gens savent ce que vous faites",
    img2a:"Les services nécessaires pour que vous ayez votre meilleur site web",
    img3a:"Des solutions complètes et adaptables à vos besoins",
    img3b:"Accompagnons votre croissance",

        //contacto and text

        texto1andwsap:"Si vous pensez que nous pouvons vous aider, n'hésitez pas à nous contacter sans engagement",

        //------------------------------------------------------------------------

     //text Home de Home

     servicios:"Prestations de service",
     textLi1:"Web adaptable à tous les appareils",
     textLi2:"Mise à jour du contenu",
     textLi3:"Gestion de domaine complète",
     textLi4:"E-mail professionnel (@votreDomaine)",
     textLi5:"des applications Web",
     prestador:"En tant que fournisseur de services numériques, notre objectif est d'aider les entrepreneurs à améliorer leur présence numérique et à atteindre leurs objectifs en ligne. Cela peut inclure la conception de sites Web, le marketing numérique, la publicité sur les réseaux sociaux, l'optimisation des moteurs de recherche (SEO), la gestion de la réputation en ligne, et bien plus encore.",
 
     //---------------------------

     ingresName:"Entrez votre nom",
     ingresEmail:"Entrez votre adresse e-mail",
     ingresConsul:"Entrez votre demande",
    //--------------------------
    enviar:"Envoyer",
    sendExit:"Le formulaire a été soumis avec succès, nous répondrons à votre demande dans les plus brefs délais.",
    camposVacios: "Aucun champ ne peut rester vide",
    validEmail:"Le format de l’e-mail n’est pas valide, veuillez vérifier et réessayer",

};
  
  export default messages;
  