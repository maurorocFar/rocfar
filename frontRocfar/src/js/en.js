// en.js
const messages = {
    title: "Welcome",
    aboutme: "About us",
    p1: "Born in the year 2022, our bases are based on providing digital solutions to daily problems that arise in society.",
    p2: "With the use of technologies, it is intended to empower the individual, either by expanding their clients or improving their services if they have a business, as well as starting new projects with a solid digital framework.",
    p3: "Without a doubt, technology has advanced rapidly in recent times and is here to stay. In this context, it is evident that those businesses that decide to implement digital technologies are more likely to experience significant growth. The adoption of technological solutions has become a key competitive advantage in today's market, making it possible to optimize processes, improve efficiency and offer more personalized experiences to customers.",
    p4a: "Ultimately since",
    p4b: "We work on our own projects, which are born from the vision of promoting people's lives, achieving objectives, setting goals and fulfilling purposes.", 
    // otras cadenas de texto en inglés
        //-----------------------------------------------

    //Comienzo Variables Footer

    tecnologies: "Technologies",
    redes: "social networks",
    derechos: "Copyright ©",
    tel:"phone",

        //---------------------------
    //Comienzo variables NavBAr

    inicio: "Home",
    about: "About Us",
    contact: "Contact",

       //---------------------------
    //Carrusel textos

    img1a:"We are also interested",
    img1b:"that more people know what you do",
    img2a:"The necessary services for you to have your best website",
    img3a:"Comprehensive and adaptable solutions to your needs",
    img3b:"Let us join your growth",

        //contacto and text

        texto1andwsap:"If you think we can help you, do not hesitate to contact us without obligation",

        //------------------------------------------------------------------------

     //text Home de Home

     servicios:"Services",
     textLi1:"Web adaptable to all devices",
     textLi2:"Content update",
     textLi3:"Comprehensive domain management",
     textLi4:"Professional Email (@yourDomain)",
     textLi5:"Web applications",
     prestador:"As a digital service provider, our goal is to help entrepreneurs improve their digital presence and achieve their online goals. This can include web designs, digital marketing, social media advertising, search engine optimization (SEO), online reputation management, and much more.",
 
     //---------------------------

     // texto de componente Form consulta.


    ingresName:"Enter your name",
    ingresEmail:"Enter your email address",
    ingresConsul:"Enter Your Inquiry",

    //--------------------------

    enviar:"send",
    sendExit:"Le formulaire a été soumis avec succès, nous répondrons à votre demande dans les plus brefs délais.",
    camposVacios: "No field can remain empty",
    validEmail:"Email format is invalid, please check and try again",
  };
  
  export default messages;
  