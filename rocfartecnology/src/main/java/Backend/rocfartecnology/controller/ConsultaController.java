package Backend.rocfartecnology.controller;

import Backend.rocfartecnology.entity.Consulta;
import Backend.rocfartecnology.service.ConsultaServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rocfar/consulta")
@CrossOrigin(origins = "*")
public class ConsultaController {


@Autowired
private ConsultaServiceI consultaServiceI;


    @PostMapping("/saveConsulta")
    public ResponseEntity<String> crearConsulta(Consulta consulta){
        consultaServiceI.save(consulta);
        return ResponseEntity.status(HttpStatus.CREATED).body("Su consulta ah sido recibida con exito, a la brevedad le responderemos al email ingresado");
    }

    @GetMapping("/traerconsultas")
    public ResponseEntity<List<Consulta>> getConsulta(){
        return ResponseEntity.ok(consultaServiceI.getConsultas());
    }

}
